part of game;


class KeybarBlock extends Block {
  KeybarBlock(Mesh mesh, Vector3 position) : super(mesh, position, 0.4);



  @override
  void onCollision(Entity entity) {
    if (entity is Player) {
      if (entity.hasKey()) {
        Level.levelFinished();
      }
    }
  }
}
