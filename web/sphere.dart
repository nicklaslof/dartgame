part of game;

class Sphere extends Entity {
  Mesh mesh;
  Vector3 newPosition = new Vector3.zero();
  bool reverse = false;
  double speed = 0.03;

  Sphere(this.mesh) : super(0, 0.0) {
    aabbSize = 0.09;
  }

  
  @override
  void update() {
    super.update();

    newPosition.setFrom(mesh.position);
    if (reverse) {
      newPosition.x += speed;
    } else {
      newPosition.x -= speed;
    }

    if (canMove(newPosition)) {
      mesh.position.setFrom(newPosition);
      updatePositionByVector3(newPosition);
    } else {
      reverse = !reverse;
      //Audio.play(Audio.ballBounce);
    }
  }
  
  @override
  void onCollision(Entity entity){
  }
}
