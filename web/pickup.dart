part of game;

abstract class PickUp extends BillboardSprite {
  double timeElapsed = 0.0;
  double amplitude = 0.1;
  double omega = 3.9;
  Vector3 itemPosition = new Vector3.zero();
  PointLight light;
  double counter = 0.0;
  double yOffset = 0.0;

  PickUp(Mesh mesh, this.light) : super(mesh, 0.0, null, null, 0, 0.0) {
    yOffset = mesh.position.y;
    aabbSize = 0.5;
  }


  /**
   * y = A*sin(omega*t)
   */

  @override
  void update() {
    super.update();

    if (mesh.visible) {
      super.update();
      timeElapsed += 0.03;

      itemPosition.y = amplitude * Math.sin(omega * timeElapsed) + yOffset;
      mesh.position.y = itemPosition.y;
      light.intensity = (itemPosition.y * 10).abs();
      updatePositionByDouble(mesh.position.x, mesh.position.z);
    }
  }
  
  @override
  bool hit(double angle){
    return false;
  }

  @override
  void onCollision(Entity entity) {
    if (entity is Player && mesh.visible) {
      mesh.visible = false;
      light.visible = false;
      pickup();
    }
  }

  void pickup();
}
