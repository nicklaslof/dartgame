part of game;


class MoveableBlock extends Entity {
  Mesh mesh;
  bool catched = false;
  Vector3 anglePosition = new Vector3.zero();
  Vector3 knockBack = new Vector3.zero();
  Vector3 blockMovement = new Vector3.zero();
  Object3D moveObject = new Object3D();

  double timeElapsed = 0.0;
  double amplitude = 0.1;
  double omega = 3.9;
  Vector3 itemPosition = new Vector3.zero();
  double yOffset = 0.0;


  MoveableBlock(this.mesh) : super(0, 0.0) {
    updatePositionByVector3(mesh.position);
    aabbSize = 0.3;
  }

  @override
  bool canPass(Entity entity) {
    if (catched && (entity is Player || entity is MoveableBlock)) {
      return true;
    }
    return false;
  }

  @override
  bool hit(double angle) {
    print("hit" + angle.toString());
    anglePosition.setValues(Math.cos(angle), 0.0, Math.sin(angle));
    knockBack.scale(0.0);
    knockBack.setFrom(anglePosition);
    return false;
  }

  @override
  void update() {
    if (!catched) {
      blockMovement.scale(0.0);
      blockMovement.x -= knockBack.x / 10;
      blockMovement.z -= knockBack.z / 10;
      knockBack.scale(0.9);

      moveObject.position.setFrom(mesh.position);
      moveObject.translateX(blockMovement.x);
      moveObject.translateZ(blockMovement.z);

      if (canMove(moveObject.position)) {
        if (!catched) {
          mesh.position.setFrom(moveObject.position);
          updatePositionByVector3(mesh.position);
        }
      } else {
        knockBack.x = -moveObject.position.x / 30;
        knockBack.z = -moveObject.position.z / 30;
      }
    } else {
      if (amplitude != 0.0) {
        timeElapsed += 0.03;
        itemPosition.y = amplitude * Math.sin(omega * timeElapsed) + yOffset;
        mesh.position.y = itemPosition.y;
        if (amplitude < 0.0) {
          amplitude = 0.0;
        } else {
          amplitude -= 0.001;
        }
      }
    }
  }

  void onCollision(Entity entity) {
  }


  void setCatchedByLava(Vector3 lavaPos) {
    if (!catched) {
      catched = true;
      mesh.position.setValues(lavaPos.x, -0.7, lavaPos.z);
      yOffset = mesh.position.y;
      updatePositionByVector3(mesh.position);
    }
  }
}
