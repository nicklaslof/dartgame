part of game;

class Materials {

  static Material wallMaterial;
  static Material holeMaterial;
  static Material floorMaterial;
  static Material stoneMaterial;
  static Material lavaMaterial;
  static Material spehreMaterialYellow;
  static Material spehreMaterialRed;
  static Material spehreMaterialBlue;
  static Material barsMaterial;
  static Material barsKeyMaterial;
  static Material golem1Material;
  static Material golem2Material;
  static Material swordMaterial;
  static Material keyMaterial;
  static Material bat1Material;
  static Material bat1Material_Hit;
  static Material bat2Material;

  static init() {
    var t = ImageUtils.loadTexture('wall.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    t.repeat.setValues(2.0, 4.0);
    wallMaterial = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x555555, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0);

    t = ImageUtils.loadTexture('transp.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    holeMaterial = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x555555, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true, opacity: 100);



    t = ImageUtils.loadTexture('bars.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    t.repeat.setValues(1.0, 1.0);

    barsMaterial = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x222222, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true);

    t = ImageUtils.loadTexture('bars_key.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    t.repeat.setValues(1.0, 1.0);

    barsKeyMaterial = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x222222, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true);



    t = ImageUtils.loadTexture('golem1.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    t.flipY = false;
    t.repeat.setValues(1.0, 1.0);

    golem1Material = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x000000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true);

    t = ImageUtils.loadTexture('golem2.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    t.flipY = false;
    t.repeat.setValues(1.0, 1.0);

    golem2Material = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x000000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true);



    t = ImageUtils.loadTexture('bat1.png');
    t.wrapS = t.wrapT = ClampToEdgeWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    //t.repeat.setValues(1.0, 1.0);

    bat1Material = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x000000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true, depthWrite: false);

    t = ImageUtils.loadTexture('bat1.png');
    t.wrapS = t.wrapT = ClampToEdgeWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    //t.repeat.setValues(1.0, 1.0);

    bat1Material_Hit = new MeshBasicMaterial(map: t, color: 0xFF0000, shading: NoShading, reflectivity: 0, transparent: true, depthWrite: false);



    t = ImageUtils.loadTexture('bat2.png');
    t.wrapS = t.wrapT = ClampToEdgeWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    bat2Material = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x000000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true, depthWrite: false);

    t = ImageUtils.loadTexture('sword.png');
    t.wrapS = t.wrapT = ClampToEdgeWrapping;
    t.magFilter = t.minFilter = NearestFilter;

    swordMaterial = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x000000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true);


    t = ImageUtils.loadTexture('key.png');
    t.wrapS = t.wrapT = ClampToEdgeWrapping;
    t.magFilter = t.minFilter = NearestFilter;

    keyMaterial = new MeshPhongMaterial(map: t, color: 0xB3CEE2, ambient: 0x000000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0, transparent: true);



    t = ImageUtils.loadTexture('floor.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    t.repeat.setValues(256.0, 256.0);
    floorMaterial = new MeshPhongMaterial(map: t, color: 0xA3BED2, ambient: 0x222222, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0);

    t = ImageUtils.loadTexture('stone.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    stoneMaterial = new MeshPhongMaterial(map: t, color: 0xA3BED2, ambient: 0x222222, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: -1);


    t = ImageUtils.loadTexture('lava.png');
    t.wrapS = t.wrapT = RepeatWrapping;
    t.magFilter = t.minFilter = NearestFilter;
    lavaMaterial = new MeshPhongMaterial(map: t, color: 0xff9c00, ambient: 0x222222, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0);




    spehreMaterialYellow = new MeshPhongMaterial(color: 0xA3BE00, ambient: 0xA3BE00, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0);
    spehreMaterialRed = new MeshPhongMaterial(color: 0xA30000, ambient: 0xA30000, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0);
    spehreMaterialBlue = new MeshPhongMaterial(color: 0x0000A3, ambient: 0x0000A3, shininess: 0, specular: 0x000000, shading: NoShading, reflectivity: 0);

  }
}
