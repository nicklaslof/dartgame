part of game;

class Lava extends Entity {
  Mesh mesh;
  double timeElapsed = 0.0;
  double amplitude = 1.3;
  double omega = 0.5;
  bool catchedStone = false;
  static num _raiseLevel = 0.03;

  Lava(this.mesh) : super(0, 0.0) {
    aabbSize = 0.01;
    updatePositionByVector3(mesh.position);

  }

  @override
  void update() {
    super.update();
  }

  void onCollision(Entity entity) {
    if (!catchedStone && entity is MoveableBlock) {
      entity.setCatchedByLava(mesh.position);
      catchedStone = true;
      updatePositionByVector3(mesh.position);
      Level.updateNeighborEntities(this, 1, new List());
    }

    if (!catchedStone && entity is Player) {
      entity.setCatchedByLava(mesh.position);
    }
  }

  @override
  void onNeighbourUpdate(Entity entity, int data, List<Entity> batchUpdateList) {
    super.onNeighbourUpdate(entity, data, batchUpdateList);
   
    switch (data) {
      case 1:
        mesh.position.y += _raiseLevel;
        updatePositionByVector3(mesh.position);
        Level.updateNeighborEntities(this, 1, batchUpdateList);

    }
  }
}
