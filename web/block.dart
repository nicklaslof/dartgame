part of game;


class Block{
  Aabb3 aabb = new Aabb3();
  Mesh mesh;
  
  
  Block(this.mesh, Vector3 position, [double aabbsize = 0.2]){
    aabb.min.setValues(position.x - aabbsize, position.y + 0.0, position.z - aabbsize);
    aabb.max.setValues(position.x + aabbsize, position.y + 1.0, position.z + aabbsize);
  }
  
  void onCollision(Entity entity){
    
  }
  
  bool canEntityPass(Entity entity){
    return false;
  }
}

class LavaBlock extends Block{
  LavaBlock(Mesh mesh, Vector3 position) : super(mesh, position);
  
  @override
  bool canEntityPass(Entity entity){
    return true;
  }
}