part of game;


class MainLoop {
  static int TICKS_PER_SECOND = 60;
  static int SKIP_TICKS = 1000 ~/ TICKS_PER_SECOND;
  static int MAX_FRAMESKIP = 10;

  num next_game_tick = 0.0;
  int loops;
  int lastTicks = 0;
  int entitiesUpdated = 0;
  double timeSpentWithRender = 0.0;
  double timeSpentWithTicks = 0.0;
  double browserTime = 0.0;

  Function onLevelFinished;
  Stopwatch watch = new Stopwatch();
  Stopwatch totalWatch = new Stopwatch();

  //FPS-counter:
  int frameCounter = 0;
  double deltaTime = 0.0;
  int fps = 0;

  MainLoop(Function onLevelFinished) {
    print("starting mainloop");
    this.onLevelFinished = onLevelFinished;
  }

  void run() {
    //window.animationFrame.then(init);
    loop(0.0);
  }

  void init(num time) {
    next_game_tick = time;
  }


  void loop(num time) {
    window.animationFrame.then(loop);
    watch.stop();
    browserTime += watch.elapsed.inMicroseconds / 1000;
    var delta = time - lastTime;
    lastTime = time;

    frameCounter++;
    deltaTime += delta;
    if (deltaTime > (1.0 / updateRate) * 1000) {
      totalWatch.stop();
      double totalTime = totalWatch.elapsedMicroseconds / 1000;
      print("fps: " + frameCounter.toString() + " ticks: " + lastTicks.toString() + " updates:" + entitiesUpdated.toString() + "/" + Level.entities.length.toString() + "  ticktime: " + timeSpentWithTicks.round().toString() + "  rendertime: " + timeSpentWithRender.round().toString() + "  vsync/browser-time:  " + browserTime.round().toString() + "  total: " + totalTime.toString());
      lastTicks = 0;
      entitiesUpdated = 0;
      timeSpentWithRender = 0.0;
      timeSpentWithTicks = 0.0;
      browserTime = 0.0;

      fps = frameCounter;
      frameCounter = 0;
      deltaTime -= (1.0 / updateRate) * 1000;
      updateShownFPS();
      totalWatch.reset();
      totalWatch.start();
    }


    loops = 0;

    watch.reset();
    watch.start();
    while (time > next_game_tick && loops < MAX_FRAMESKIP) {
      if (!Level.finished) {
        for (int i = 0; i < Level.entities.length; i++) {
          //if (Level.entities[i] is Player) continue;
          Level.entities[i].update();
          entitiesUpdated++;
        }
      }
      next_game_tick += SKIP_TICKS;
      loops++;
    }

    watch.stop();
    timeSpentWithTicks += watch.elapsed.inMicroseconds / 1000;

    lastTicks += loops;

    Level.updatePlayerHealth();

    watch.reset();
    watch.start();
    renderer.autoClear = true;
    renderer.render(scene, camera);
    renderer.autoClear = false;
    renderer.render(uiScene, uiCamera);
    watch.stop();

    timeSpentWithRender += watch.elapsed.inMicroseconds / 1000;

    if (Level.finished) {
      onLevelFinished();
      return;
    }
    watch.reset();
    watch.start();

  }

  void updateShownFPS() {
    if (fpselement != null) {
      fpselement.text = fps.toString() + " fps";
    }
  }

}
