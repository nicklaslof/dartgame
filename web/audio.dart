part of game;


class Audio {

  static String HIT = "hit";
  static String PLAYER_HIT = "playerHit";
  static String DEAD = "dead";
  static String GOT_ITEM = "gotItem";
  static String BALLBOUNCE = "ballBounce";

  static stagexl.ResourceManager _resourceManager;

  static init() {
    print("Loading audiofiles");

    _resourceManager = new stagexl.ResourceManager();

    _resourceManager.addSound(HIT, "hit_hurt15.wav");
    _resourceManager.addSound(DEAD, "explosion8.wav");
    _resourceManager.addSound(GOT_ITEM, "powerup50.wav");
    _resourceManager.addSound(BALLBOUNCE, "hit_hurt49.wav");
    _resourceManager.addSound(PLAYER_HIT, "hit_hurt73.wav");
    _resourceManager.load();
  }


  static play(String soundName) {
    var sound = _resourceManager.getSound(soundName);
    var soundTransform = new stagexl.SoundTransform(0.5);
    var soundChannel = sound.play(false, soundTransform);
  }
}
