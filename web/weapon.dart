part of game;

abstract class Weapon extends Item{
  Vector3 attackPosition = new Vector3.zero();
  Vector3 anglePosition = new Vector3.zero();
  Mesh attackHelper;

  Weapon() {
    //attackHelper = new Mesh(Geometries.sphere, Materials.spehreMaterialRed);
    //scene.add(attackHelper);

  }

  void attack(Scene scene, Vector3 position, double angle) {
    attackPosition.setFrom(position);

    anglePosition.setValues(Math.cos(angle), 0.0, Math.sin(angle));
    anglePosition.normalize();
    anglePosition.scale(0.5);
    attackPosition.sub(anglePosition);

   // attackHelper.position.setFrom(attackPosition);

    for (int i = 0; i < Level.entities.length; i++) {
      var entity = Level.entities[i];
      if (entity.aabb.intersectsWithVector3(attackPosition)) {
        if (!entity.dead) {
          entity.hit(angle);
        }
      }
    }

  }
}
