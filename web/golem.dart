part of game;

class Golem extends BillboardSprite {

  Vector3 newPosition = new Vector3.zero();
  bool moveback = false;
  double speed = 0.4;

  List<Material> animation = new List(2);

  Golem(mesh) : super(mesh, 0.5, Materials.golem1Material, Materials.golem2Material, 5, 0.5) {
    this.animate = false;
  }

  @override
  void update() {
    super.update();
    /*

    mesh.rotation.setFrom(Level.player.camera.rotation);

    if (this.animate) {
      newPosition.setFrom(mesh.position);
      if (moveback) {
        newPosition.z += speed * delta;
      } else {
        newPosition.z -= speed * delta;
      }

      if (canMove(newPosition)) {
        mesh.position.setFrom(newPosition);
        updatePositionByVector3(newPosition);
      } else {
        if (moveback) {
          moveback = false;
          this.animate = false;
        }
      }
    }*/


    if (Level.player.position.distanceTo(mesh.position) < 3.0) {
      if (!moveback) {
        this.animate = true;
      }
    } else {
      if (this.animate) {
        moveback = true;
      }
    }


  }
}
