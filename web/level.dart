part of game;


class Level {

  static const String SPAWNPOSITION = '00ff00';
  static const String SOLIDWALL = "ffffff";
  static const String BARS = "00ffff";
  static const String BARS_KEY = "006666";
  static const String GOLEM = "646464";
  static const String BAT = "323232";
  static const String SWORD = "ff00ff";
  static const String KEY = "888800";

  static const String LAVA = "ff6c00";
  static const String LAVA_LIGHT = "ff9c00";
  static const String LAVA_BORDER = "707070";
  static const String LAVA_BORDER_WALL = "808080";

  static const String MOVEABLE_BLOCK = "919191";

  static const String SPEHRE_YELLOW = "ffff00";
  static const String SPEHRE_RED = "ff0000";
  static const String SPEHRE_BLUE = "0000ff";

  static Map<int, Map<int, Block>> map;
  //static List<Block> blocks = new List();
  static List<Entity> entities;
  static Player player;
  static Scene uiScene;

  static bool finished = false;


  static Mesh playerHealth;
  static int playerLastHealth = 0;


  Future loadFonts() => Future.wait(["helvetiker_regular.json"].map((path) => HttpRequest.getString(path).then((data) {
    FontUtils.loadFace(JSON.decode(data));
  })));

  Level(Scene scene, Scene uiScene, Player playerInstance, String levelName) {
    finished = false;
    entities = new List();

    player = null;
    player = playerInstance;
    Level.uiScene = uiScene;

    map = new Map<int, Map<int, Block>>();

    for (int x0 = 0; x0 < 65; x0++) {
      if (!map.containsKey(x0)) {
        Map newMap = new Map<int, Block>();
        map[x0] = newMap;
      }
    }

    ImageElement image = new ImageElement(src: "levels/" + levelName + ".png");
    var futures = [image.onLoad.first];
    Future.wait(futures).then((_) {
      print("loading " + levelName);
      var pixels = getPixels(image);
      buildLevel(scene, pixels, false);
      buildLevel(scene, pixels, true);
      loadFonts().then((_) {

      });

      entities.add(player);
    });
  }



  void buildLevel(Scene scene, ImageData pixels, bool alphaPass) {
    var pixelData = pixels.data;
    int x = 64;
    int y = 64;
    int xPos = 64;
    int yPos = 64;

    for (int i = pixelData.length - 1; i > -1; i -= 4) {
      var r = pixelData[i - 3];
      var g = pixelData[i - 2];
      var b = pixelData[i - 1];
      var a = pixelData[i];

      var rgbToHexString = C.Color.rgbToHexString(r, g, b);

      // This can probably be done in a muuuuuuuuch better way
      if (y == 0) {
        y = 64;
        yPos--;
      }

      if (xPos == 0) {
        xPos = 64;
      }

      xPos--;
      y--;
      // This can probably be done in a muuuuuuuuch better way


      if (!alphaPass) {
        switch (rgbToHexString) {

          case SPAWNPOSITION:
            player.setPosition(xPos.toDouble() / 2.0, yPos.toDouble() / 2.0);
            break;

          case SOLIDWALL:
            // This should be rebuilt using better meshes. Now we are wasting lot of verticies (and number of meshes)
            // on two sides of each wall which are not visible.
            var mesh = new Mesh(Geometries.wall, Materials.wallMaterial)..position.setValues(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0);
            mesh.matrixAutoUpdate = false;
            mesh.renderDepth = 254;
            mesh.updateMatrix();
            scene.add(mesh);
            setBlock(xPos, yPos, new Block(mesh, new Vector3(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0)));
            break;

          case LAVA:
          case LAVA_LIGHT:
            // Ugly hack for the floor..  will be removed when we can build proper floor mesh
            //*******************
            var mesh = new Mesh(Geometries.wall, Materials.holeMaterial)..position.setValues(xPos.toDouble() / 2.0, -0.99, yPos.toDouble() / 2.0);
            mesh.matrixAutoUpdate = false;
            mesh.renderDepth = 255;
            mesh.updateMatrix();
            scene.add(mesh);
            //*******************

            mesh = new Mesh(Geometries.wall, Materials.lavaMaterial)..position.setValues(xPos.toDouble() / 2.0, -1.2, yPos.toDouble() / 2.0);
            mesh.renderDepth = 256;
            if (rgbToHexString == LAVA_LIGHT) {
              var pointLight = new PointLight(0xff9c00, intensity: 2, distance: 3.0)..position.setValues(0.0, 1.0, 0.0);
              mesh.add(pointLight);
            }

            scene.add(mesh);
            //setBlock(xPos, yPos, new LavaBlock(mesh, new Vector3(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0)));
            var lava = new Lava(mesh);
            entities.add(lava);
            break;
          case LAVA_BORDER:
          case LAVA_BORDER_WALL:
            addLavaContainerBrick(xPos, yPos, scene);
            if (rgbToHexString == LAVA_BORDER_WALL) {
              var mesh = new Mesh(Geometries.wall, Materials.wallMaterial)..position.setValues(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0);
              mesh.matrixAutoUpdate = false;
              mesh.renderDepth = 254;
              mesh.updateMatrix();
              scene.add(mesh);
              setBlock(xPos, yPos, new Block(mesh, new Vector3(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0)));
            }
            break;

          case MOVEABLE_BLOCK:
            var mesh = new Mesh(Geometries.halfCube, Materials.stoneMaterial)..position.setValues(xPos.toDouble() / 2.0, -0.35, yPos.toDouble() / 2.0);
            mesh.renderDepth = 256;
            scene.add(mesh);

            var moveableBlock = new MoveableBlock(mesh);
            entities.add(moveableBlock);

            break;

          case SPEHRE_YELLOW:
          case SPEHRE_BLUE:
          case SPEHRE_RED:
            Material material;
            num color;
            if (rgbToHexString == SPEHRE_YELLOW) material = Materials.spehreMaterialYellow;
            if (rgbToHexString == SPEHRE_RED) material = Materials.spehreMaterialRed;
            if (rgbToHexString == SPEHRE_BLUE) material = Materials.spehreMaterialBlue;

            var mesh = new Mesh(Geometries.sphere, material)..position.setValues(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0);

            var pointLight = new PointLight((material as MeshPhongMaterial).color.getHex(), intensity: 3, distance: 2.0)..position.setValues(0.0, 0.0, 0.0);

            mesh.add(pointLight);
            scene.add(mesh);

            var sphere = new Sphere(mesh);
            entities.add(sphere);
            break;
        }


      } else {
        switch (rgbToHexString) {
          case BARS:
            var mesh = new Mesh(Geometries.bars, Materials.barsMaterial)..position.setValues(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0);
            mesh.matrixAutoUpdate = false;
            mesh.renderDepth = 253;
            mesh.updateMatrix();
            scene.add(mesh);

            setBlock(xPos, yPos, new Block(mesh, new Vector3(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0)));
            break;

          case BARS_KEY:
            var mesh = new Mesh(Geometries.bars, Materials.barsKeyMaterial)..position.setValues(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0);
            mesh.matrixAutoUpdate = false;
            mesh.renderDepth = 253;
            mesh.updateMatrix();
            scene.add(mesh);

            setBlock(xPos, yPos, new KeybarBlock(mesh, new Vector3(xPos.toDouble() / 2.0, 0.0, yPos.toDouble() / 2.0)));
            break;

          /*case GOLEM:
            var mesh = new Mesh(Geometries.bigMonster, Materials.golem1Material)..position.setValues(xPos.toDouble() / 2.0, -0.15, yPos.toDouble() / 2.0);
            scene.add(mesh);

            var golem = new Golem(mesh);
            entities.add(golem);
            break;*/

          case BAT:
            var mesh = new Mesh(Geometries.smallMonster, Materials.bat1Material)..position.setValues(xPos.toDouble() / 2.0, -0.15, yPos.toDouble() / 2.0);
            scene.add(mesh);
            mesh.renderDepth = 252;
            var bat = new Bat(mesh);
            entities.add(bat);
            break;


          case SWORD:
            var mesh = new Mesh(Geometries.bigMonster, Materials.swordMaterial)
                ..position.setValues(xPos.toDouble() / 2.0, -0.15, yPos.toDouble() / 2.0)
                ..rotation.z = Math.PI / 2.0;

            var pointLight = new PointLight(0x0096ff, intensity: 3, distance: 2.0)..position.setValues(0.0, 0.0, 0.5);
            mesh.renderDepth = 252;
            mesh.add(pointLight);
            scene.add(mesh);

            var sword = new Sword(mesh, pointLight);
            entities.add(sword);
            break;

          case KEY:
            var mesh = new Mesh(Geometries.bigMonster, Materials.keyMaterial)
                ..position.setValues(xPos.toDouble() / 2.0, -0.15, yPos.toDouble() / 2.0)
                ..rotation.z = Math.PI / 2.0;

            var pointLight = new PointLight(0xdecc38, intensity: 3, distance: 2.0)..position.setValues(0.0, 0.0, 0.5);
            mesh.renderDepth = 253;
            mesh.add(pointLight);
            scene.add(mesh);

            var key = new LockKey(mesh, pointLight);
            entities.add(key);
            break;

        }
      }
    }

    if (!alphaPass) {
      var mesh = new Mesh(Geometries.plane, Materials.floorMaterial)..position.setValues(0.0, -0.55, 0.0);

      scene.add(mesh);
      mesh.renderDepth = 254;

      mesh = new Mesh(Geometries.plane, Materials.floorMaterial)..position.setValues(0.0, 0.55, 0.0);
      scene.add(mesh);
      mesh.renderDepth = 254;

      // Ugly hack for the floor..  will be removed when we can build proper floor mesh
      for (int x = 0; x < 64; x++) {
        for (int z = 0; z < 64; z++) {
          var block = getBlock(x, z);
          if (block is LavaBlock) {
            var b = getBlock(x + 1, z);
            if (!(b is LavaBlock)) {
              addLavaContainerBrick(x + 1, z, scene);
            }
            b = getBlock(x - 1, z);
            if (!(b is LavaBlock)) {
              addLavaContainerBrick(x - 1, z, scene);
            }
            b = getBlock(x, z + 1);
            if (!(b is LavaBlock)) {
              addLavaContainerBrick(x, z + 1, scene);
            }

            b = getBlock(x, z - 1);
            if (!(b is LavaBlock)) {
              addLavaContainerBrick(x, z - 1, scene);
            }
          }
        }
      }
    }

  }

  void addLavaContainerBrick(num x, num z, Scene scene) {
    var mesh = new Mesh(Geometries.halfCube, Materials.wallMaterial)..position.setValues((x.toDouble()) / 2.0, -0.649, z.toDouble() / 2.0);
    mesh.matrixAutoUpdate = false;
    mesh.renderDepth = 256;
    mesh.updateMatrix();
    scene.add(mesh);

  }

  // Get image pixels from image element.
  ImageData getPixels(ImageElement img) {
    var canvas = new CanvasElement(width: img.width, height: img.height);
    CanvasRenderingContext2D context = canvas.getContext('2d');
    context.drawImage(img, 0, 0);
    return context.getImageData(0, 0, canvas.width, canvas.height);
  }

  static Block getBlock(int x0, int y0) {
    if (x0 < 0 || x0 > 65 || y0 < 0 || y0 > 65) return null;
    return map[x0][y0];
  }

  static void setBlock(int x0, int y0, Block block) {
    map[x0][y0] = block;
    //blocks.add(block);
  }


  //TODO: Move this font stuff to seperate class

  static void levelFinished() {
    var fontshapes = FontUtils.generateShapes("Level 1 finished!!", 50);

    MeshBasicMaterial fontmaterial = new MeshBasicMaterial(color: 0xff0000, side: DoubleSide);

    ShapeGeometry fontgeometry = new ShapeGeometry(fontshapes, curveSegments: 20);

    var fontmesh = new Mesh(fontgeometry, fontmaterial);
    fontmesh.position.setValues(-200.0, 0.00, -5.0);

    uiScene.add(fontmesh);
    finished = true;

  }


  //TODO: Move this font stuff to seperate class

  static void updatePlayerHealth() {
    try {
      if (playerHealth == null || player.health != playerLastHealth) {
        var fontshapes = FontUtils.generateShapes("Health: " + player.health.toString(), 25);

        MeshBasicMaterial fontmaterial = new MeshBasicMaterial(color: 0x999999, side: DoubleSide);

        ShapeGeometry fontgeometry = new ShapeGeometry(fontshapes, curveSegments: 20);
        if (playerHealth != null) {
          uiScene.remove(playerHealth);
        }
        playerHealth = new Mesh(fontgeometry, fontmaterial);
        playerHealth.position.setValues(-(window.innerWidth.toDouble() / 2.0) + 30.0, 350.00, -5.0);

        uiScene.add(playerHealth);
        playerLastHealth = player.health;
      }
    } catch (Exception) {

    }
  }


  static void gameOver() {
    var fontshapes = FontUtils.generateShapes("Game over!!", 50);

    MeshBasicMaterial fontmaterial = new MeshBasicMaterial(color: 0xff0000, side: DoubleSide);

    ShapeGeometry fontgeometry = new ShapeGeometry(fontshapes, curveSegments: 20);

    var fontmesh = new Mesh(fontgeometry, fontmaterial);
    fontmesh.position.setValues(-200.0, 0.00, -5.0);

    uiScene.add(fontmesh);
    finished = true;
  }
  
  static void updateNeighborEntities(Entity entity, int data, [List<Entity> batchUpdateList]) {
    if (batchUpdateList == null){
      batchUpdateList = new List();
    }
    var neighborEntities = entities.where((e) =>
      
      e.runtimeType == entity.runtimeType && 
          !batchUpdateList.contains(e) &&
      (
          e.position.distanceTo(entity.position) < 1.0
      )
    
    ).toSet();
    
    neighborEntities.forEach((e)=> batchUpdateList.add(e));
    
    neighborEntities.forEach((e)=> e.onNeighbourUpdate(entity, data, batchUpdateList));
    
    
  }
}
