part of game;


class Player extends Entity {
  Vector3 moveVector = new Vector3.zero();
  Vector3 newPosition = new Vector3.zero();
  Vector3 movement = new Vector3.zero();
  Object3D moveObject = new Object3D();
  Vector3 hitAngle = new Vector3.zero();
  Vector3 knockBack = new Vector3.zero();
  Vector3 anglePosition = new Vector3.zero();
  Camera camera;
  Mesh swordMesh;
  double swordRotX;
  double swordRotY;
  double swordPosZ;
  bool haveAttacked;

  num movementSpeed = 0.0030;
  num lookSpeed = 0.06;
  num swordSpeed = 8.0;

  SwordItem swordItem;
  Scene scene;
  List<Item> items = new List<Item>();

  Player(Scene scene, Camera camera) : super(7, 1.5) {
    this.camera = camera;
    aabbSize = 0.2;
    this.scene = scene;
  }

  void gotSword() {
    swordMesh = new Mesh(Geometries.bigMonster, Materials.swordMaterial)
        ..position.setValues(0.05, -0.04, -0.1)
        ..renderDepth = -10
        ..scale.setValues(0.2, 0.2, 0.2)
        ..doubleSided = true
        ..frustumCulled = false
        ..rotation.z = -Math.PI / 2.0
        ..rotation.y -= 0.5;
    camera.add(swordMesh);

    swordRotX = swordMesh.rotation.x;
    swordRotY = swordMesh.rotation.y;
    swordPosZ = swordMesh.position.z;

    PointLight pointlight = new PointLight(0x0096ff, intensity: 2, distance: 5.0);
    pointlight.position.setValues(0.0, 0.0, 0.0);
    pointlight.castShadow = false;
    camera.add(pointlight);
    swordItem = new SwordItem();
    items.add(swordItem);
  }


  void gotKey() {
    var keyItem = new KeyItem();
    items.add(keyItem);
  }

  bool hasKey() {
    return (items.any((Item item) => item is KeyItem));
  }

  @override
  bool hit(double angle) {
    var hit = super.hit(angle);
    if (hit) {
      anglePosition.setValues(Math.sin(angle), 0.0, Math.cos(angle));
      knockBack.scale(0.0);
      knockBack.setFrom(anglePosition);
      knockBack.scale(0.7);
      Audio.play(Audio.PLAYER_HIT);
      if (dead){
        Level.gameOver();
      }
    }

    return hit;
  }

  setPosition(double x, double z) {
    camera.position.setValues(x, 0.0, z);
    updatePositionByDouble(x, z);
    moveObject.position.setFrom(position);
  }

  @override
  bool canPass(Entity entity) {

    if (entity is Sphere) {
      return false;
    }

    return true;
  }

  @override
  void onCollision(Entity entity) {
    if (entity is Sphere) {
      var sub = entity.position.clone().sub(position.clone());
      var angle = Math.atan2(sub.x, sub.z);
      hit(angle);
    }
  }

  @override
  update() {
    super.update();
    if (Level.finished || dead) {
      return;
    }

    Map<int, bool> cpk = currentlyPressedKeys;
    Map<String, bool> cpmb = currentlyPressedMouseButtons;

    moveVector.scale(0.0);

    if (cpk[Key.W] != null || cpk[Key.UP] != null) {
      moveVector.z -= movementSpeed;
    }
    if (cpk[Key.A] != null) {
      moveVector.x -= movementSpeed;
    }
    if (cpk[Key.S] != null || cpk[Key.DOWN] != null) {
      moveVector.z += movementSpeed;
    }
    if (cpk[Key.D] != null) {
      moveVector.x += movementSpeed;
    }

    if (cpk[Key.RIGHT] != null) {
      camera.rotation.y -= lookSpeed;
      moveObject.rotation.y -= lookSpeed;
    }
    if (cpk[Key.LEFT] != null) {
      camera.rotation.y += lookSpeed;
      moveObject.rotation.y += lookSpeed;
    }

    if (swordMesh != null) {
      if (cpk[Key.SPACE] != null) {
        bool swordAttacked = false;
        if (swordMesh.rotation.y > -1.0) {
          swordMesh.rotation.y -= swordSpeed;
          if (swordMesh.rotation.y < -1.0) {
            swordMesh.rotation.y = -1.0;
          }
        }
        if (swordMesh.rotation.x > -0.6) {
          swordMesh.rotation.x -= swordSpeed;
          if (swordMesh.rotation.x < -0.6) {
            swordMesh.rotation.x = -0.6;
          }
        }

        if (swordMesh.position.z > -0.11) {
          swordMesh.position.z -= (swordSpeed / 100);
          if (swordMesh.position.z < -0.11) {
            swordMesh.position.z = -0.11;
            swordAttacked = true;
          }
        }

        if (!haveAttacked && swordAttacked) {
          haveAttacked = true;
          var angle = Math.atan2(camera.matrixWorldInverse.storage[0], camera.matrixWorldInverse.storage[2]);
          swordItem.attack(scene, position, angle);
        }


      } else {
        swordMesh.rotation.x = swordRotX;
        swordMesh.rotation.y = swordRotY;
        swordMesh.position.z = swordPosZ;
        haveAttacked = false;
      }
    }


    moveObject.position.setFrom(camera.position);
    moveObject.rotation.setFrom(camera.rotation);
    moveObject.matrix.setFrom(camera.matrix);
    moveObject.translateX(moveVector.x);
    moveObject.translateZ(moveVector.z);
    moveObject.translateX(movement.x);
    moveObject.translateZ(movement.z);
    moveObject.translateX(-knockBack.x);
    moveObject.translateZ(-knockBack.z);
    
    knockBack.scale(0.3);

    if (canMove(moveObject.position)) {
      movement.sub(knockBack);
      movement.add(moveVector);
      if (movement.x != 0 || movement.z != 0) {
        camera.translateX(movement.x);
        camera.translateZ(movement.z);
        updatePositionByVector3(camera.position);
        movement.scale(0.9);
      }
    } else {
      movement.x = -moveVector.x * 2;
      movement.z = -moveVector.z * 2;
    }


  }
  
  void setCatchedByLava(Vector3 lavaPos) {
      camera.position.setValues(lavaPos.x, -0.5, lavaPos.z);
      updatePositionByVector3(camera.position);
    }
  }



abstract class Key {

  static int LEFT = 37;
  static int RIGHT = 39;
  static int UP = 38;
  static int DOWN = 40;
  static int SPACE = 32;
  static int SHIFT = 16;
  static int CTRL = 17;
  static int ALT = 18;
  static int TAB = 9;
  static int A = 65;
  static int B = 66;
  static int C = 67;
  static int D = 68;
  static int E = 69;
  static int F = 70;
  static int G = 71;
  static int H = 72;
  static int I = 73;
  static int J = 74;
  static int K = 75;
  static int L = 76;
  static int M = 77;
  static int N = 78;
  static int O = 79;
  static int P = 80;
  static int Q = 81;
  static int R = 82;
  static int S = 83;
  static int T = 84;
  static int U = 85;
  static int V = 86;
  static int W = 87;
  static int X = 88;
  static int Y = 89;
  static int Z = 90;
}
