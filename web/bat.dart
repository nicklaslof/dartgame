part of game;

class Bat extends BillboardSprite {


  double timeElapsed = 0.0;
  double amplitude = 1.3;
  double omega = 0.5;

  double xOffset;
  double zOffset;


  Object3D moveObject = new Object3D();

  Vector3 tmp = new Vector3.zero();
  Vector3 anglePosition = new Vector3.zero();
  Vector3 batMovement = new Vector3.zero();
  Matrix4 tmpM = new Matrix4.zero();
  Vector3 knockBack = new Vector3.zero();

  bool positive = true;

  Bat(mesh) : super(mesh, 0.25, Materials.bat1Material, Materials.bat2Material, 3, 0.25) {
    aabbSize = 0.3;
    this.animate = true;
    xOffset = mesh.position.x;
    zOffset = mesh.position.z;
  }

  @override
  bool hit(double angle) {
    var hit = super.hit(angle);
    if (!dead && hit) {
      Audio.play(Audio.HIT);
      anglePosition.setValues(Math.cos(angle), 0.0, Math.sin(angle));
      knockBack.scale(0.0);
      knockBack.setFrom(anglePosition);
      //knockBack.scale(5.0);

      //print(knockBack);
    }
    if (dead){
      Audio.play(Audio.DEAD);
    }
    return hit;
  }

  @override
  bool canPass(Entity entity) {
    if (entity is Player) {
      return true;
    }

    if (entity is Sphere) {
      return true;
    }

    return false;
  }

  @override
  void update() {
    super.update();
    
    if (dead) {
      return;
    }

    timeElapsed += 0.01;

    batMovement.x = (amplitude * Math.sin(omega + 0.3 * timeElapsed)) / 60;
    batMovement.z = (amplitude * Math.cos(omega - 0.2 * timeElapsed)) / 60;
    if (positive) {
      batMovement.x -= knockBack.x/10;
      batMovement.z -= knockBack.z/10;
    } else {
      batMovement.x += knockBack.x/10;
      batMovement.z += knockBack.z/10;
    }

    knockBack.scale(0.9);


    moveObject.position.setFrom(mesh.position);
    if (positive) {
      moveObject.translateX(batMovement.x);
      moveObject.translateZ(batMovement.z);
    } else {
      moveObject.translateX(-batMovement.x);
      moveObject.translateZ(-batMovement.z);
    }


    if (canMove(moveObject.position)) {
      mesh.position.setFrom(moveObject.position);
      updatePositionByVector3(mesh.position);
    } else {
      positive = !positive;
    }
    

  }
}
