part of game;

class Sword extends PickUp {

  Sword(Mesh mesh, Light light) : super(mesh, light) {

  }

  @override
  void update() {
    super.update();
  }

  @override
  void pickup() {
    Level.player.gotSword();
    Audio.play(Audio.GOT_ITEM);
  }
}
