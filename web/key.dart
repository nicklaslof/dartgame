part of game;

class LockKey extends PickUp {
  LockKey(Mesh mesh, Light light) : super(mesh, light);


  @override
  void pickup() {
    Level.player.gotKey();
    Audio.play(Audio.GOT_ITEM);
  }
}
