part of game;

abstract class Entity {

  Aabb3 aabb = new Aabb3();
  Aabb3 newPositionaabb = new Aabb3();
  Vector3 position = new Vector3.zero();
  double aabbSize = 0.2;
  bool dead = false;
  int maxHealth = 0;
  int health;
  double hitCountdown = 0.5;
  double hitCountTimer = 0.0;

  Entity(this.maxHealth, this.hitCountdown) {
    health = maxHealth;
  }

  void update(){
    if (hitCountTimer > 0.0){
      hitCountTimer -= 0.01;
    }else{
      hitCountTimer = 0.0;
    }
  }

  bool canMove(Vector3 newPosition) {
    newPositionaabb.min.setValues(newPosition.x - aabbSize, newPosition.y + 0.0, newPosition.z - aabbSize);
    newPositionaabb.max.setValues(newPosition.x + aabbSize, newPosition.y + 1.0, newPosition.z + aabbSize);

    var posX = (newPosition.x * 2.0).floor();
    var posZ = (newPosition.z * 2.0).floor();

    for (int curPosX = posX - 1; curPosX < posX + 2; curPosX++) {
      for (int curPosZ = posZ - 2; curPosZ < posZ + 2; curPosZ++) {
        var block = Level.getBlock(curPosX, curPosZ);
        if (block != null) {
          if (block.aabb.intersectsWithAabb3(newPositionaabb)) {
            block.onCollision(this);
            return false;
          }
        }
      }
    }

    for (int i = 0; i < Level.entities.length; i++) {
      var entity = Level.entities[i];
      if (entity != this) {
        if (entity.aabb.intersectsWithAabb3(newPositionaabb)) {
          entity.onCollision(this);
          return entity.canPass(this);
        }
      }
    }

    return true;
  }

  void updatePositionByDouble(double x, double z) {
    position.x = x;
    position.z = z;
    aabb.min.setValues(position.x - aabbSize, position.y + 0.0, position.z - aabbSize);
    aabb.max.setValues(position.x + aabbSize, position.y + 1.0, position.z + aabbSize);
  }

  void updatePositionByVector3(Vector3 newPosition) {
    position.x = newPosition.x;
    position.z = newPosition.z;
    aabb.min.setValues(position.x - aabbSize, position.y + 0.0, position.z - aabbSize);
    aabb.max.setValues(position.x + aabbSize, position.y + 1.0, position.z + aabbSize);

  }

  bool hit(double angle) {
    if (hitCountTimer > 0.0) return false;
    
    health--;
    print(health);
    if (health <= 0) {
      dead = true;
    }
    hitCountTimer = hitCountdown;
    
    return true;
  }
  
  void onCollision(Entity entity) {
  }
  
  bool canPass(Entity entity) {
    return true;
  }
  
  void onNeighbourUpdate(Entity entity, int data, List<Entity> broadcastUpdated) {
  }
}
