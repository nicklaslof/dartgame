part of game;

class Geometries{
  
  static Geometry wall;
  static Geometry halfCube;
  static Geometry bars;
  static Geometry bigMonster;
  static Geometry smallMonster;
  static Geometry plane;
  static Geometry sphere;
  
  static Geometry healthBar;
  
  static init(){
    wall = new CubeGeometry(0.5, 1.0, 0.5);
    halfCube = new CubeGeometry(0.5, 0.25, 0.5);
    bars = new CubeGeometry(0.5, 1.0, 0.5);
    bigMonster = new PlaneGeometry(0.5, 0.7);
    smallMonster = new PlaneGeometry(0.3, 0.3);
    plane = new CubeGeometry(46.0, 0.1, 32.0, 16,1,16);
    sphere = new SphereGeometry(0.05);
    
    healthBar = new CubeGeometry(0.1, 0.00001, 0.01);
  }
}