library game;

import 'dart:html';
import 'package:three/three.dart';
import 'package:three/extras/font_utils.dart' as FontUtils;
import 'package:vector_math/vector_math.dart';
import 'package:three/extras/image_utils.dart' as ImageUtils;
import 'package:color/color.dart' as C;
import 'dart:async';
import 'dart:math' as Math;
import 'package:stagexl/stagexl.dart' as stagexl;
import 'dart:convert';

part 'mainloop.dart';
part 'level.dart';
part 'block.dart';
part 'material.dart';
part 'geometries.dart';
part 'player.dart';
part 'entity.dart';
part 'audio.dart';

part 'sphere.dart';
part 'billboardsprite.dart';
part 'golem.dart';
part 'bat.dart';
part 'sword.dart';
part 'weapon.dart';
part 'sworditem.dart';
part 'pickup.dart';
part 'item.dart';
part 'keybarblock.dart';
part 'key.dart';
part 'keyitem.dart';
part 'moveableblock.dart';
part 'lava.dart';

Element container;
Scene scene;
Scene uiScene;
WebGLRenderer renderer;
PerspectiveCamera camera;
OrthographicCamera uiCamera;
Light ambientLight;


double updateRate = 1.0;
Element fpselement;
Element webglelement;

Player player;

Map<int, bool> currentlyPressedKeys = new Map<int, bool>();
Map<String, bool> currentlyPressedMouseButtons = new Map<String, bool>();
double lastTime = 0.0;
List<String> levels = new List<String>();
int currentLevel = 0;

void main() {
  levels.add("start");
  levels.add("level2");
  init();
}


void init() {
  fpselement = document.querySelector('#fps');
  webglelement = document.querySelector('#webgl-canvas');

  document.onKeyDown.listen((KeyboardEvent e) {
    currentlyPressedKeys[e.keyCode] = true;
  });
  document.onKeyUp.listen((KeyboardEvent e) {
    currentlyPressedKeys[e.keyCode] = null;
  });


  renderer = new WebGLRenderer(antialias: false, canvas: webglelement)
      ..setClearColorHex(0x000000, 1)
      ..sortObjects = true
      ..alpha = true;
  
  renderer.setFaceCulling(CullFaceBack, FrontFaceDirectionCCW);
  renderer.precision = "highp";
  renderer.setSize( window.innerWidth-30, window.innerHeight-30 );
  Materials.init();
  Geometries.init();
  Audio.init();



  camera = new PerspectiveCamera(60.0, (window.innerWidth-30) / (window.innerHeight-30), 0.005, 25.0);
  camera.position.z = 10.0;

  ambientLight = new AmbientLight(0x333333);
  window.onResize.listen(onWindowResize);
  changeLevel();

}


onWindowResize(e) {
  
  camera.aspect = (window.innerWidth-30) / (window.innerHeight-30);
  uiCamera.left = -(window.innerWidth+30) / 2;
  uiCamera.right = (window.innerWidth-30) / 2;
  uiCamera.top = (window.innerHeight-30) / 2;
  uiCamera.bottom = -(window.innerHeight+30) / 2;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth-30, window.innerHeight-30 );

}

changeLevel() {
  scene = new Scene();
  scene.add(camera);
  scene.add(ambientLight);
  uiScene = new Scene();
  uiCamera = new OrthographicCamera(-(window.innerWidth+30) / 2, (window.innerWidth-30) / 2, (window.innerHeight-30) / 2, -(window.innerHeight+30) / 2, 0.0, 2000.0);
  player = new Player(scene, camera);
  currentLevel++;
  var level = new Level(scene, uiScene, player, levels[currentLevel]);
  var mainLoop = new MainLoop(changeLevel);
  mainLoop.run();
}


