part of game;


class BillboardSprite extends Entity {

  Mesh mesh;
  List<Material> animation = new List(2);
  double _timeElapsed = 0.0;
  double _animationSpeed;
  bool animate = false;
  int frame = 0;
  Mesh healthBar;

  BillboardSprite(this.mesh, this._animationSpeed, Material m1, Material m2, int maxHealth, double hitCountdown) : super(maxHealth, hitCountdown) {
    animation[0] = m1;
    animation[1] = m2;
    if (m1 == null || m2 == null) {
      animate = false;
    }

    if (health > 0) {

      var meshBasicMaterial = new MeshBasicMaterial(color: 0x00FF00, shading: NoShading);

      healthBar = new Mesh(Geometries.healthBar, meshBasicMaterial);
      mesh.add(healthBar);


      healthBar.position.setValues(0.0, 0.2, 0.0);
      healthBar.scale.y = 0.01;
    }
    
    updatePositionByVector3(mesh.position);
  }


  @override
  bool hit(double angle) {
    var hit = super.hit(angle);
    if (hit && dead) {
      mesh.visible = false;
      Level.entities.remove(this);
      if (healthBar != null) healthBar.visible = false;
    } else {
      mesh.material = Materials.bat1Material_Hit;
    }

    return hit;
  }

  @override
  void update() {
    super.update();
    if (dead) {
      return;
    }
    mesh.lookAt(Level.player.position);

    updateHealthBar();
    if (animate) {
      _timeElapsed += 0.01;

      if (_timeElapsed >= _animationSpeed) {
        _timeElapsed = 0.0;
        if (frame == 0) frame = 1; else frame = 0;
        mesh.material = animation[frame];
      }
    }
    mesh.updateMatrixWorld(force: true);
  }

  void updateHealthBar() {
    if (healthBar == null) return;
    healthBar.rotation.x = -mesh.rotation.x + 1.0;
    double size = health / maxHealth;
    healthBar.scale.x = size;
    if (size <= 0.34) {
      (healthBar.material as MeshBasicMaterial).color = new Color(0xFF0000);
    } else if (size <= 0.67) {
      (healthBar.material as MeshBasicMaterial).color = new Color(0xffff00);
    } else {
      (healthBar.material as MeshBasicMaterial).color = new Color(0x00FF00);
    }
  }
}
